@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Maintenance Listing') }} 
                <a style="float:right" type="button" href="/vechile-maintenance/create"> + Add Vechile Maintenance</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                            <tr>
                                <th class="center"> Sr.No</th>
                                <th class="center">Vechile</th>
                                <th class="center">Date & Time</th>
                                <th class="center">Service</th>
                                <th class="center">Price</th>
                                <th class="center" colspan="2">Actions</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <tbody>
                            @if(count($vechileMaintenance))
                            @php $itr = 1;@endphp
                            @foreach($vechileMaintenance as $item)
                            <tr data-id="">
                                <td class="center">{{$itr}}</td>
                                <td class="center">{{$item->vechile->name ?? '' }}</td>

                                <td class="center">{{\Carbon\Carbon::parse($item->dateTime)->format('j F Y h:i A')}}</td>
                                <td class="center">{{$item->service_by}}</td>
                                <td class="center">$ {{$item->charges}}</td>
                                <td class="center">
                                    <a type="button" href="/vechile-maintenance/{{$item->id}}/edit">Edit</a>
                                </td>
                                <td class="center">
                                    <form action="/vechile-maintenance/{{$item->id}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @php $itr++;@endphp
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5">
                                    No record found
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection