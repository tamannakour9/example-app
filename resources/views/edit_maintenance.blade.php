@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><b>Update Vehicle Maintenance</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="post" action="/vechile-maintenance/{{$vechileMaintenance->id}}" enctype="multipart/form-data" was-validated>
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Category</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="category" value="{{$vechileMaintenance->vechile->category ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Vechile name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="name" value="{{$vechileMaintenance->vechile->name ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1"> Vechile model</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="model" value="{{$vechileMaintenance->vechile->model ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Vechile brand </label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="brand" value="{{$vechileMaintenance->vechile->brand ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">registeration Number</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="registeration_number" value="{{$vechileMaintenance->vechile->registeration_number ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Service by</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="service_by" value = "{{$vechileMaintenance->service_by ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Charges</label>
                            <input type="number" class="form-control" id="exampleFormControlInput1" name="charges" value = "{{$vechileMaintenance->charges ?? ''}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker' class="datetimepicker">
                                <input type='text' class="form-control" name="date_time" placeholder="{{$vechileMaintenance->dateTime ?? ''}}"  class="datetimepicker" value="{{$vechileMaintenance->dateTime ?? ''}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection