@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><b>Add Vehicle Maintenance</b></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="post" action="/vechile-maintenance" enctype="multipart/form-data" was-validated>
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Category</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="category" placeholder="Enter Category" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Vechile name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="name" placeholder="Enter Vechile name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1"> Vechile model</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="model" placeholder="Enter Vechile model" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Vechile brand </label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="brand" placeholder="Enter Vechile brand" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Registeration Number</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="registeration_number" placeholder="Enter Registeration Number" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Service by</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="service_by" placeholder="Enter Service by" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Charges</label>
                            <input type="number" class="form-control" id="exampleFormControlInput1" name="charges" placeholder="Enter Charges" required>
                        </div>
                        <div class="form-group">
                        <label for="exampleFormControlInput1">Date & Time</label>
                            <div class='input-group date' id='datetimepicker'>
                                <input type='text' class="form-control" name="date_time"  placeholder="Enter Date & Time" required/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection