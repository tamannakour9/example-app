<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'category',
        'name',
        'model',
        'brand',
        'registeration_number',
        'user_id'
    ];

    /**
     * Get's this user's vote
     */
    public function user()
    {
        return $this->hasOne(\App\Models\user::class,'id','user_id');
    }
}
