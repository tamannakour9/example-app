<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'dateTime',
        'service_by',
        'charges',
        'vehicle_id'
    ];

    /**
     * Get's this user's vote
     */
    public function vechile()
    {
        return $this->hasOne(\App\Models\Vehicle::class,'id','vehicle_id');
    }
}
