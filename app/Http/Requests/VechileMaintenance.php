<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VechileMaintenance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'category' => 'required|string|min:4|max:255',
        ];
    }
}
