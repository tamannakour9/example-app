<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Maintenance as MaintenanceService;

class VehicleMaintenanceController extends Controller
{
    public function __construct()
    {
        $this->vechileMaintenance = new MaintenanceService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vechileMaintenance = $this->vechileMaintenance->index();
        return view('home', compact('vechileMaintenance'));
    }

    public function edit($id)
    {
        $vechileMaintenance = $this->vechileMaintenance->find($id);
        return view('edit_maintenance', compact('vechileMaintenance'));
    }
    public function create()
    {
        $vechileMaintenance = $this->vechileMaintenance->index();
        return view('add_maintenance', compact('vechileMaintenance'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vechileMaintenance = $this->vechileMaintenance->store($request);
        if ($vechileMaintenance) {
            $message = trans('message.vechile_maintenance_added');
        } else {
            $message = trans('message.problem_occured_while_adding');
        }
        return redirect('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vechileMaintenance = $this->vechileMaintenance->update($request, $id);
        if ($vechileMaintenance) {
            $message = trans('message.vechile_maintenance_updated');
        } else {
            $message = trans('message.problem_occured_while_updating');
        }
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vechileMaintenance = $this->vechileMaintenance->destroy($id);
        if ($vechileMaintenance) {
            $message = trans('message.vechileMaintenance_deleted');
        } else {
            $message = trans('message.problem_occured_while_deleting');
        }
        return redirect('/home');
    }
}
