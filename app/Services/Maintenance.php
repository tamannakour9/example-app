<?php

namespace App\Services;

use App\Models\Maintenance as MaintenanceModel;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Maintenance
{
    //get Maintenance
    public function index()
    {
        $startDate = Carbon::today();
        $endDate = Carbon::today()->addDays(7);
        $vechileMaintenance = MaintenanceModel::with('vechile')->whereBetween('dateTime', [$startDate, $endDate])->get();
        return $vechileMaintenance;
    }

    //store Maintenance
    public function store($request)
    {
        $vechile = Vehicle::create([
            'category' => $request->category ?? '',
            'name' => $request->name ?? '',
            'model' => $request->model ?? '',
            'brand' => $request->brand ?? '',
            'registeration_number' => $request->registeration_number,
            'user_id' => Auth::user()->id,
        ]);
        if($vechile){
            $maintenance = MaintenanceModel::create([
                'dateTime' => Carbon::parse($request->date_time) ??  '',
                'service_by' => $request->service_by ?? '',
                'charges' => $request->charges ?? '',
                'vehicle_id' => $vechile->id,
            ]);
            if ($maintenance) {
                return $maintenance;
            }
        }

    }

    //update Maintenance
    public function update($request, $id)
    {
        $maintenance = MaintenanceModel::with('vechile')->find($id);
        if ($maintenance) {
            $updated = $maintenance->update([
                'dateTime' => Carbon::parse($request->date_time) ?? '',
                'service_by' => $request->service_by ?? '',
                'charges' => $request->charges ?? '',
            ]);
            if($updated){
                $vechile = $maintenance->vechile()->updateOrCreate([
                    'user_id'   => Auth::user()->id,
                ],[
                    'category' => $request->category ?? '',
                    'name' => $request->name ?? '',
                    'model' => $request->model ?? '',
                    'brand' => $request->brand ?? '',
                    'registeration_number' => $request->registeration_number,
                ]);
            }
            if ($vechile) {
                return $maintenance;
            }
        }
    }

    //delete Maintenance
    public function destroy($id)
    {
        $maintenance = MaintenanceModel::find($id);
        if ($maintenance) {
            $maintenance->vechile()->delete();
            $response = $maintenance->delete();
            if ($response) {
                return $response;
            }
        }
    }

    public function find($id)
    {
        $maintenance = MaintenanceModel::find($id);
        return $maintenance;
    }

}
