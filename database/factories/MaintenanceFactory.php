<?php

namespace Database\Factories;

use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Maintenances>
 */
class MaintenanceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'dateTime' => fake()->dateTimeBetween('now', '+10 days'),
            'service_by' => fake()->name(),
            'charges' =>  $this->faker->unique()->randomNumber(3),
            'created_at' => now(),
            'vehicle_id'=> Vehicle::all()->random()->id
        ];
    }
}
