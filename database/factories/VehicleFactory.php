<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vehicles>
 */
class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'category' => fake()->title(),
            'model' => fake()->name(),
            'brand' => fake()->name(),
            'user_id' => User::all()->random()->id,
            'registeration_number' => fake()->title(),
            'created_at' => now()
        ];
    }
}
